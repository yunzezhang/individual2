FROM rust:1.75 as builder
WORKDIR /usr/src/myapp
COPY . .
RUN cargo install --path .

FROM debian:buster-slim

COPY --from=builder /usr/local/cargo/bin/rust_microservice /usr/local/bin/rust_microservice
CMD ["rust_microservice"]
