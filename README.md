# Rust Microservice with Docker and CI/CD Pipeline

This project demonstrates a simple REST API developed in Rust using the Actix-web framework. It showcases how to containerize a Rust-based microservice with Docker and automate the build and deployment process using a CI/CD pipeline.


## Getting Started

To get started with this project, clone the repository to your local machine:

```bash
git clone https://gitlab.com/yunzezhang/individual2.git
cd rust_microservice
cargo build
```

### Running Locally

Ensure you have Rust installed on your system. You can then run the service locally by executing:

```bash
cargo run
```

The service will be available at `http://localhost:8080/`.

### Using Docker

To build and run the service using Docker, execute the following commands:

```bash
docker build -t rust_microservice .
docker run -d -p 8080:8080 rust_microservice
```

The service will be accessible at `http://localhost:8080/`.

### CI/CD Pipeline

The `.gitlab-ci.yml` file in the repository defines the CI/CD pipeline stages for building the Docker image and deploying the microservice. The pipeline automates the process of pushing the Docker image to the registry and deploying it.

## Dockerfile

The `Dockerfile` provided in the project consists of a multi-stage build process that first compiles the Rust application and then packages it into a slim Debian-based image for runtime.

## Demo Video Link

https://drive.google.com/file/d/14gxtlCDxi-LGD-dGaQV790dwHY3olE42/view?usp=sharing